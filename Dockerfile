FROM openjdk:8-jre-alpine
EXPOSE 8080
WORKDIR /app
COPY target/api.jar api.jar
CMD java -jar api.jar