# Integrator

Integrate with apps like Slack

## Slack Integration flow

![](docs/Slack_App_Integration.png)

## Slack Setup

### Prerequisites

Slack workspace with at least one channel and access to add apps

### Slack App Setup

Create a Slack app with the following:

1.  Enable Events
    *  Request URL should the events endpoint of the Integrator app. In this 
    case http://integrator.us-east-2.elasticbeanstalk.com/slack/events.
    *  Subscribe to bot events - reaction_added event

2.  Slash Commands - /hello

3.  Incoming Webhooks
    *  Activate Incoming Webhooks and Add New Webhook to the channel


## AWS Setup and Deployment

See [this](https://gitlab.com/sandeeprai/books/-/blob/master/README.md).