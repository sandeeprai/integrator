package com.sr.integrator;

import java.io.IOException;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.slack.api.bolt.App;
import com.slack.api.methods.response.chat.ChatPostMessageResponse;
import com.slack.api.model.event.ReactionAddedEvent;

@Configuration
public class SlackApp {
	@Bean
	public App initSlackApp() throws IOException {
		App app = new App();
		
		app.command("/hello", (req, ctx) -> {
			return ctx.ack("What's up " + req.getPayload().getUserName() + "!");
		});
		
		app.event(ReactionAddedEvent.class, (payload, ctx) -> {
			  ReactionAddedEvent event = payload.getEvent();
			  if (event.getReaction().equals("white_check_mark")) {
			    ChatPostMessageResponse message = ctx.client().chatPostMessage(r -> r
			      .channel(event.getItem().getChannel())
			      .threadTs(event.getItem().getTs())
			      .text("<@" + event.getUser() + "> Thank you! We greatly appreciate your efforts :two_hearts:"));
			    if (!message.isOk()) {
			      ctx.logger.error("chat.postMessage failed: {}", message.getError());
			    }
			  }
			  return ctx.ack();
			});
		
		return app;
	}
}
