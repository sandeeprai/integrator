package com.sr.integrator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@SpringBootApplication
@ServletComponentScan
public class Integrator {

	public static void main(String[] args) {
		SpringApplication.run(Integrator.class, args);
	}

}
