package com.sr.integrator.context.event;

import javax.servlet.ServletContextListener;

import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Config {
	
	@Bean
	ServletListenerRegistrationBean<ServletContextListener> servletListener() {
	    ServletListenerRegistrationBean<ServletContextListener> srb
	      = new ServletListenerRegistrationBean<>();
	    srb.setListener(new AppContextEventSender());
	    return srb;
	}
}

