package com.sr.integrator.context.event;

import java.io.IOException;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.slack.api.Slack;

public class AppContextEventSender
  implements ServletContextListener {
	
	private String webhookUrl = System.getenv("SLACK_WEBHOOK_URL");
 
    @Override
    public void contextDestroyed(ServletContextEvent event) {
    	Slack slack = Slack.getInstance();

		String payload = "{\"text\":\"I'm going down, Bye!\"}";

		try {
			slack.send(webhookUrl, payload);
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
 
    @Override
    public void contextInitialized(ServletContextEvent event) {
    	Slack slack = Slack.getInstance();

		String payload = "{\"text\":\"Hello, I'm up and running!\"}";

		try {
			slack.send(webhookUrl, payload);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}